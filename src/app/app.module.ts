import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatButtonModule, MatCardModule, MatDividerModule, MatIconModule, MatExpansionModule } from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { DasboardPanelComponent } from '@view/dasboard-panel/dasboard-panel.component';
import { CreateCustomerComponent } from '@view/create-customer/create-customer.component';
import { CarouselComponent } from '@view/carousel/carousel.component';
import { InfoCardsComponent } from '@view/info-cards/info-cards.component';
import { MenunavBarComponent } from '@view/menunav-bar/menunav-bar.component';
import { ExpansionPanelComponent } from '@components/expansion-panel/expansion-panel.component';
import { AboutPageComponent } from '@view/about-page/about-page.component';
import { PageLayoutTemplateComponent } from '@components/page-layout-template/page-layout-template.component';

@NgModule({
  declarations: [
    AppComponent,
    DasboardPanelComponent,
    CreateCustomerComponent,
    CarouselComponent,
    InfoCardsComponent,
    MenunavBarComponent,
    ExpansionPanelComponent,
    AboutPageComponent,
    PageLayoutTemplateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgSelectModule,
    HttpClientModule,
    AppRoutingModule,
    MatInputModule,
    NgbModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatExpansionModule,
    // NgbCarousel,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
