import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<any>{
    return this.http.get(`${environment.api}/users`);
  }

  saveCustomer(formData): Observable<any>{
    console.log(`${environment.api}/users`);
    return this.http.get(`${environment.api}/users`);
  }
}
