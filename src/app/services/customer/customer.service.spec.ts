import { TestBed } from '@angular/core/testing';

import { SaveCustomerService } from './save-customer.service';

describe('SaveCustomerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveCustomerService = TestBed.get(SaveCustomerService);
    expect(service).toBeTruthy();
  });
});
