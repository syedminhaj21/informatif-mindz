import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenunavBarComponent } from './menunav-bar.component';

describe('MenunavBarComponent', () => {
  let component: MenunavBarComponent;
  let fixture: ComponentFixture<MenunavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenunavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenunavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
