import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menunav-bar',
  templateUrl: './menunav-bar.component.html',
  styleUrls: ['./menunav-bar.component.scss']
})
export class MenunavBarComponent implements OnInit {
  title = "Innovatif Mindz";
  
  constructor() { }

  ngOnInit() {
  }

}
