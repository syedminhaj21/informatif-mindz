import { Component, OnInit } from '@angular/core';
import { Location} from '@angular/common'
import { Customer } from '../../services/customer-datatype'
import { NgOption } from '@ng-select/ng-select';
import { MatInputModule } from '@angular/material';

import { CustomerService } from '../../services/customer/customer.service'

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.scss']
})
export class CreateCustomerComponent implements OnInit {
  
  formData: Customer;
  cities: NgOption[] = [
    {id: 1, name: 'Hyderabad'},
    {id: 2, name: 'Pune'},
    {id: 3, name: 'Mumbai'},
    {id:4 , name:'Delhi'}
  ]

  constructor(
    private location: Location,
    private customerService: CustomerService
  ) { }

  ngOnInit() {
    this.formData = { name: '', email: '', city: ''};
  }

  onSubmit(){
    console.log("values", this.formData)
    this.customerService.saveCustomer(this.formData);
  }

  onBackClick(){
    this.location.back();
  }

}
