import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.scss']
})
export class AboutPageComponent implements OnInit {
  private imagePath: string;
  private infoText = "In today's dynamic business environment, organizations need to continuously streamline business processes. Businesses have to handle the challenges posed by digitization and globalization along with other issues such as data deluge, resource management systems, growing customer expectations and complex business processes. Robust ERP systems with cloud based technology and streamline business processes can help them tackle most of these problems and increase efficiency and gain productive achievements. With numerous cloud computing delivery models to choose from, businesses are struggling to settle on the best option that addresses their existing challenges with their on-premise systems, At Innovatif Mindz we help you judge the benefits of SAP cloud platform and offer solutions with SAP consultation, implementation, global rollout, migration, and support services that help enterprises enhance SAP functionalities to achieve maximum ROI. This ensures that the cloud infrastructure implementation is smooth, seamless and harmonious. Innovatif Mindz  has seasoned team of SAP R/3, ECC,SAP Cloud based S/4HANA, SAPUI5 and SAP Netweaver experts who has extensive experience in SAP implementation and consulting. Innovatif Mindz is working in ERP domain serving different industries and customers segments";
  constructor() { 
    this.imagePath = '../../../assets/aboutus.jpg';
  }

  ngOnInit() {
  }

}
