import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dasboard-panel',
  templateUrl: './dasboard-panel.component.html',
  styleUrls: ['./dasboard-panel.component.scss']
})
export class DasboardPanelComponent implements OnInit {

  items=[{ displayName:'Create new Customer', route:'create'}, { displayName:'Create Master Car Entry', route: 'master'}]
  constructor() { }

  ngOnInit() {
  }

}
