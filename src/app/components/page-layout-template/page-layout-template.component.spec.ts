import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageLayoutTemplateComponent } from './page-layout-template.component';

describe('PageLayoutTemplateComponent', () => {
  let component: PageLayoutTemplateComponent;
  let fixture: ComponentFixture<PageLayoutTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageLayoutTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageLayoutTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
