import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'page-layout-template',
  templateUrl: './page-layout-template.component.html',
  styleUrls: ['./page-layout-template.component.scss']
})
export class PageLayoutTemplateComponent implements OnInit {
  @Input('infoText') infoText: string;
  imagePath: string;
  constructor() {
    this.imagePath = '../../../assets/aboutus.jpg';
   }

  ngOnInit() {
  }

}
