import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AppModule } from '../app.module'
import { DasboardPanelComponent } from '@view/dasboard-panel/dasboard-panel.component'
import { CreateCustomerComponent } from '@view/create-customer/create-customer.component'
import { AboutPageComponent } from '@view/about-page/about-page.component'

const routes:Routes = [
  {path: 'create', component: CreateCustomerComponent},
  {path: 'dashboard', component: DasboardPanelComponent},
  {path: 'aboutus', component: AboutPageComponent},
  {path: '', redirectTo:'/dashboard' , pathMatch: 'full'}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
